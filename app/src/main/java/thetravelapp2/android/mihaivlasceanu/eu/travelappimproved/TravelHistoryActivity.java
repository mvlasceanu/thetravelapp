package thetravelapp2.android.mihaivlasceanu.eu.travelappimproved;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import thetravelapp2.android.mihaivlasceanu.eu.travelappimproved.DAO.TravelDAO;


public class TravelHistoryActivity extends ListActivity {

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // ListView content layout
        setContentView(R.layout.activity_travel_history);

        // Dao Instance
        TravelDAO dao = new TravelDAO(this);

        // History list
        Cursor travels = dao.getTravels();

        // Attach adapter only if records are present
        if(travels != null && travels.getCount() > 0)
        {
            TravelHistoryAdapter cursorAdapter = new TravelHistoryAdapter(this, travels);
            setListAdapter(cursorAdapter);
        } else {
            setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, DestinationActivity.EMPTY));
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_destination, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id)
    {

    }
}
