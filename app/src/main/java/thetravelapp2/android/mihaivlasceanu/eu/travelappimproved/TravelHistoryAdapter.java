package thetravelapp2.android.mihaivlasceanu.eu.travelappimproved;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import thetravelapp2.android.mihaivlasceanu.eu.travelappimproved.DAO.TravelDAO;

/**
 * Created by Rembrandt on 04-Mar-15.
 */

/**
 * Adapter for history ListView
 */
public class TravelHistoryAdapter extends CursorAdapter {

    /**
     * Constructor with Context and Cursor data
     * @param context
     * @param c
     */
    public TravelHistoryAdapter(Context context, Cursor c)
    {
        super(context, c, 0);
    }

    /**
     *
     * @param context
     * @param cursor
     * @param parent
     * @return
     */
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.item_travel_history, parent, false);
    }

    /**
     *
     * @param view
     * @param context
     * @param cursor
     */
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // Find fields to populate in inflated template
        TextView start = (TextView) view.findViewById(R.id.start);
        TextView destination = (TextView) view.findViewById(R.id.destination);
        // Extract properties from cursor
        String from = cursor.getString(cursor.getColumnIndexOrThrow(TravelDAO.TRAVEL_HISTORY_COLUMN_STATION_START));
        String dest = cursor.getString(cursor.getColumnIndexOrThrow(TravelDAO.TRAVEL_HISTORY_COLUMN_STATION_DEST));
        // Populate fields with extracted properties
        start.setText(from);
        destination.setText(String.valueOf(dest));
    }
}
