package thetravelapp2.android.mihaivlasceanu.eu.travelappimproved;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import thetravelapp2.android.mihaivlasceanu.eu.travelappimproved.DAO.TravelDAO;

/**
 * Created by Rembrandt on 04-Mar-15.
 */
public class StationListViewAdapter extends CursorAdapter {

    private Activity context;
    private Cursor cursor;
    private List<String> mSelectedItemsIds;
    private ActionMode mActionMode;

    /**
     * Constructor with Context and Cursor data
     * @param context
     * @param c
     */
    public StationListViewAdapter(Activity context, Cursor c)
    {
        super(context, c, 0);
        this.mSelectedItemsIds = new ArrayList<>();
        this.context = context;
        this.cursor = c;
    }

    /**
     *
     * @param context
     * @param cursor
     * @param parent
     * @return
     */
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.iten_station, parent, false);
    }

    /**
     *
     * @param view
     * @param context
     * @param cursor
     */
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // Find fields to populate in inflated template
        TextView station = (TextView) view.findViewById(R.id.station);
        // Extract properties from cursor
        String stationItem = cursor.getString(cursor.getColumnIndexOrThrow(TravelDAO.STATIONS_COLUMN_STATION_NAME));
        // Populate fields with extracted properties
        station.setText(stationItem);
    }

    public void toggleSelection(int position) {
        selectView(position, mSelectedItemsIds.get(position));
    }

    public void selectView(int position, String value) {
        if (value != null)
            mSelectedItemsIds.add(value);
        else
            mSelectedItemsIds.remove(position);

        notifyDataSetChanged();
    }

    public void removeSelection() {
        mSelectedItemsIds.clear();
        notifyDataSetChanged();
    }

    public void remove(Cursor object) {
        TravelDAO dao = new TravelDAO(context);
        dao.removeStation(object);
        notifyDataSetChanged();
    }

    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    public List<String> getSelectedIds() {
        return mSelectedItemsIds;
    }
}
