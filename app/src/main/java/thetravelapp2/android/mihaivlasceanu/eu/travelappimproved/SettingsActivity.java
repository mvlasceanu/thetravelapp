package thetravelapp2.android.mihaivlasceanu.eu.travelappimproved;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import thetravelapp2.android.mihaivlasceanu.eu.travelappimproved.DAO.TravelDAO;

/**
 * Settings activity
 */
public class SettingsActivity extends Activity {

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Display the fragment as the main content.
        FragmentManager mFragmentManager = getFragmentManager();
        FragmentTransaction mFragmentTransaction = mFragmentManager
                .beginTransaction();
        PrefsFragment mPrefsFragment = PrefsFragment.newInstance(new TravelDAO(this));
        mFragmentTransaction.replace(android.R.id.content, mPrefsFragment);
        mFragmentTransaction.commit();

    }

    public static class PrefsFragment extends PreferenceFragment {

        // DAO instance
        private TravelDAO travelDAO;

        /**
         * Indirect constructor passing the DAO instance
         *
         * @param travelDAO
         * @return
         */
        public static PrefsFragment newInstance(TravelDAO travelDAO)
        {
            PrefsFragment pf = new PrefsFragment();
            pf.travelDAO = travelDAO;
            return pf;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            // Load the preferences from an XML resource
            addPreferencesFromResource(R.xml.settings);

            // Click on clear history setting
            Preference clearHistory = findPreference("history_clear");
            clearHistory.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    PrefsFragment.this.travelDAO.clearHistory();
                    Toast.makeText(getActivity(), "History cleared!", Toast.LENGTH_LONG).show();
                    return true;
                }
            });

            // Click on clear stations setting
            Preference clearStations = findPreference("stations_clear");
            clearStations.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    PrefsFragment.this.travelDAO.clearStations();
                    Toast.makeText(getActivity(), "Stations deleted!", Toast.LENGTH_LONG).show();
                    return true;
                }
            });
        }
    }
}
