package thetravelapp2.android.mihaivlasceanu.eu.travelappimproved.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.ListView;

import java.util.List;

import thetravelapp2.android.mihaivlasceanu.eu.travelappimproved.helpers.DatabaseHelper;

/**
 * Created by Rembrandt on 04-Mar-15.
 */
public class TravelDAO {

    // Travel History
    public static final String TRAVEL_HISTORY_COLUMN_ID             = "_id";
    public static final String TRAVEL_HISTORY_COLUMN_STATION_START  = "start";
    public static final String TRAVEL_HISTORY_COLUMN_STATION_DEST   = "destination";

    // Stations
    public static final String STATIONS_COLUMN_ID             = "_id";
    public static final String STATIONS_COLUMN_STATION_NAME  = "station";

    // Passed context
    private Context context;

    // Helper instance
    private final DatabaseHelper helper;

    // Database connection
    private SQLiteDatabase db;

    // Preferences
    private final SharedPreferences preferences;

    /**
     * Constructor with context
     * @param context
     */
    public TravelDAO(Context context)
    {
        this.helper = new DatabaseHelper(context);
        this.context = context;
        preferences = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE);
    }

    /**
     *  Open connection for writing
     */
    public void open()
    {
        this.db = helper.getWritableDatabase();
    }

    /**
     * Close connection
     */
    public void close()
    {
        this.db.close();
    }

    /**
     * Travel history
     * @return
     */
    public Cursor getTravels()
    {
        // Open read connection
        this.db = helper.getReadableDatabase();

        // Limit results according to Preferences
        String limit = preferences.getString("history_length", "10");

        // Projection
        String projection[] = {
                TravelDAO.TRAVEL_HISTORY_COLUMN_ID,
                TravelDAO.TRAVEL_HISTORY_COLUMN_STATION_START,
                TravelDAO.TRAVEL_HISTORY_COLUMN_STATION_DEST
        };

        // Sort the results
        String sortOrder = TravelDAO.TRAVEL_HISTORY_COLUMN_ID + " DESC";

        // Cursor
        Cursor cursor = this.db.query(DatabaseHelper.TRAVEL_HISTORY_TABLE,  projection,
            null, null, null, null, sortOrder, limit);

        return cursor;
    }

    /**
     * Save travel data
     * @param start
     * @param destination
     */
    public void saveTravel(String start, String destination)
    {
        open();

        ContentValues values = new ContentValues();
        values.put(TRAVEL_HISTORY_COLUMN_STATION_START, start);
        values.put(TRAVEL_HISTORY_COLUMN_STATION_DEST, destination);

        db.insert(DatabaseHelper.TRAVEL_HISTORY_TABLE, null, values);
    }

    /**
     * Get stations
     * @return
     */
    public Cursor getStations()
    {
        this.db = helper.getReadableDatabase();

        String projection[] = {
                TravelDAO.STATIONS_COLUMN_ID,
                TravelDAO.STATIONS_COLUMN_STATION_NAME
        };

        String sortOrder = TravelDAO.STATIONS_COLUMN_STATION_NAME + " ASC";

        Cursor cursor = this.db.query(
                DatabaseHelper.STATIONS_TABLE,
                projection,
                null,
                null,
                null,
                null,
                sortOrder
        );

        return cursor;
    }

    /**
     * Save station
     * @param station
     */
    public void saveStation(String station)
    {
        open();

        ContentValues values = new ContentValues();
        values.put(STATIONS_COLUMN_STATION_NAME, station);
        if(getStationCount(station) == 0) {
            db.insertWithOnConflict(DatabaseHelper.STATIONS_TABLE, null, values, SQLiteDatabase.CONFLICT_IGNORE);
        }
    }

    public void removeStation(Cursor cursor)
    {
        open();
        db.execSQL("DELETE FROM " + DatabaseHelper.STATIONS_TABLE + " WHERE " + STATIONS_COLUMN_STATION_NAME + " = '" + cursor.getString(cursor.getColumnIndex(TravelDAO.STATIONS_COLUMN_STATION_NAME)) + "'");
    }


    /**
     * Delete all stations
     */
    public void clearStations()
    {
        open();
        db.delete(DatabaseHelper.STATIONS_TABLE, null, null);
        db.close();
    }

    /**
     * Delete all history
     */
    public void clearHistory()
    {
        open();
        db.delete(DatabaseHelper.TRAVEL_HISTORY_TABLE, null, null);
    }

    public int getStationCount(String station)
    {
        Cursor c = null;
        db = helper.getReadableDatabase();
        String query = "SELECT COUNT(*) from " + DatabaseHelper.STATIONS_TABLE + " where " + STATIONS_COLUMN_STATION_NAME + " = ?";
        c = db.rawQuery(query, new String[] {station});
        if (c.moveToFirst()) {
            return c.getInt(0);
        }
        return 0;
    }

    /**
     * Context getter
     * @return
     */
    public Context getContext() {
        return this.context;
    }
}
