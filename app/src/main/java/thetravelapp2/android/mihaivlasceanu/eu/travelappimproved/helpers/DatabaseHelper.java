package thetravelapp2.android.mihaivlasceanu.eu.travelappimproved.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import thetravelapp2.android.mihaivlasceanu.eu.travelappimproved.DAO.TravelDAO;

/**
 * Created by Rembrandt on 04-Mar-15.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    // DB name
    public static final String DB_NAME                 = "travel";

    // History table name
    public static final String TRAVEL_HISTORY_TABLE    = "travel_history";

    // Stations table name
    public static final String STATIONS_TABLE          = "stations";

    /**
     * Constructor with some default params
     * @param context
     */
    public DatabaseHelper(Context context)
    {
        super(context, DB_NAME, null, 1);
    }

    /**
     * Create DB
     *
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        // Create history table SQL
        String query_create_history_table = "CREATE TABLE " + TRAVEL_HISTORY_TABLE + " (" +
                "" + TravelDAO.TRAVEL_HISTORY_COLUMN_ID + " integer primary key autoincrement," +
                "" + TravelDAO.TRAVEL_HISTORY_COLUMN_STATION_START + " text," +
                "" + TravelDAO.TRAVEL_HISTORY_COLUMN_STATION_DEST + " text);";

        // Create stations table SQL
        String query_create_stations_table = "CREATE TABLE " + STATIONS_TABLE + " (" +
                "" + TravelDAO.STATIONS_COLUMN_ID + " integer primary key autoincrement, " +
                "" + TravelDAO.STATIONS_COLUMN_STATION_NAME + " text);";

        // Run queries
        db.execSQL(query_create_history_table);
        db.execSQL(query_create_stations_table);
    }

    /**
     *
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
