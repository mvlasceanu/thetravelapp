package thetravelapp2.android.mihaivlasceanu.eu.travelappimproved;

import android.app.ActionBar;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import thetravelapp2.android.mihaivlasceanu.eu.travelappimproved.DAO.TravelDAO;


public class DestinationActivity extends ListActivity  {
    // Empty string array
    public static final String EMPTY[] = {};

    // The adapter
    StationListViewAdapter cursorAdapter;


    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // ListView for destinations
        setContentView(R.layout.activity_destination);
        this.getListView().setSelector(R.drawable.listitem_background);
        getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        ActionBar actionBar = getActionBar();
        actionBar.hide();

        // DAO Instance
        TravelDAO dao = new TravelDAO(this);

        // Stations list
        Cursor stations = dao.getStations();

        // Set adapter only if the list is not empty
        if(stations != null && stations.getCount() > 0) {
            cursorAdapter = new StationListViewAdapter(this, stations);
            setListAdapter(cursorAdapter);
        } else {
            // Empty array
            setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, EMPTY));
        }

        ListView item = getListView();

        item.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                onLongListItemClick(view, position, id);
                return false;
            }
        });
    }

    /**
     *
     * @param view
     * @param position
     * @param id
     */
    protected void onLongListItemClick(View view, int position, long id) {
        getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        getListView().setMultiChoiceModeListener(new ModeCallback());
        getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View item,
                                           int position, long id) {
                getListView().setItemChecked(position, true);
                return true;
            }

        });
    }

    /**
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_destination, menu);

        return true;
    }

    /**
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     *
     * @param l
     * @param v
     * @param position
     * @param id
     */
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id)
    {
        super.onListItemClick(l, v, position, id);

        if(getListView().getChoiceMode() == ListView.CHOICE_MODE_SINGLE)
        {
            Cursor cursor = (Cursor) l.getItemAtPosition(position);
            String value = cursor.getString(cursor.getColumnIndexOrThrow("station"));
            Intent intent = new Intent();
            intent.putExtra(TravelActivity.SELECTED_STATION_NAME, value);
            intent.putExtra("ACTION", getIntent().getStringExtra("ACTION"));
            setResult(RESULT_FIRST_USER, intent);
            finish();
        }
    }

    /**
     *
     * @return
     */
    public StationListViewAdapter getAdapter()
    {
        return this.cursorAdapter;
    }

    /**
     *
     */
    private class ModeCallback implements ListView.MultiChoiceModeListener {

        /**
         * Edit action mode
         *
         * @param mode
         * @param menu
         * @return
         */
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_destination, menu);
            mode.setTitle("Select Items");
            return true;
        }

        /**
         *
         * @param mode
         * @param menu
         * @return
         */
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return true;
        }

        /**
         * Click on action menu buttons
         *
         * @param mode
         * @param item
         * @return
         */
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menu_delete:
                    SparseBooleanArray checked = getListView().getCheckedItemPositions();

                    for (int i = 0; i < getListView().getAdapter().getCount(); i++) {
                        TravelDAO dao = new TravelDAO(DestinationActivity.this);
                        if (checked.get(i)) {
                            Cursor cursor = (Cursor) getListView().getAdapter().getItem(i);
                            dao.removeStation(cursor);
                            getAdapter().notifyDataSetChanged();
                        }
                    }
                    mode.finish();
                    break;
                default:
                    Toast.makeText(DestinationActivity.this, "Clicked " + item.getTitle(),
                            Toast.LENGTH_SHORT).show();
                    break;
            }
            return true;
        }

        /**
         * What happens when the "Done" button is pressed, or no item is selected
         *
         * @param mode
         */
        public void onDestroyActionMode(ActionMode mode) {
            // I am sure there is a better way to do this, but I could not find one
            Intent intent = new Intent(DestinationActivity.this, DestinationActivity.class);
            startActivity(intent);
            finish();
        }

        /**
         * List item selection
         *
         * @param mode
         * @param position
         * @param id
         * @param checked
         */
        public void onItemCheckedStateChanged(ActionMode mode,
                                              int position, long id, boolean checked) {
            final int checkedCount = getListView().getCheckedItemCount();
            switch (checkedCount) {
                case 0:
                    mode.setSubtitle(null);
                    break;
                case 1:
                    mode.setSubtitle("One item selected");
                    break;
                default:
                    mode.setSubtitle("" + checkedCount + " items selected");
                    break;
            }
        }
    }
}
