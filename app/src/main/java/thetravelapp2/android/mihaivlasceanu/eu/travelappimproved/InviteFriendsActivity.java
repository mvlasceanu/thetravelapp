package thetravelapp2.android.mihaivlasceanu.eu.travelappimproved;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.ListActivity;
import android.app.ListFragment;
import android.app.LoaderManager;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import thetravelapp2.android.mihaivlasceanu.eu.travelappimproved.DAO.TravelDAO;


public class InviteFriendsActivity extends ListActivity  {

    private final static String[] FROM_COLUMNS = {
            Build.VERSION.SDK_INT
                    >= Build.VERSION_CODES.HONEYCOMB ?
                    ContactsContract.Contacts.DISPLAY_NAME_PRIMARY :
                    ContactsContract.Contacts.DISPLAY_NAME
    };

    /*
     * Defines an array that contains resource ids for the layout views
     * that get the Cursor column contents. The id is pre-defined in
     * the Android framework, so it is prefaced with "android.R.id"
     */
    private final static int[] TO_IDS = {
            R.id.friend
    };
    // Define global mutable variables
    // Define a ListView object
    ListView mContactsList;
    // Define variables for the contact the user selects
    // The contact's _ID value
    long mContactId;
    // The contact's LOOKUP_KEY
    String mContactKey;
    // A content URI for the selected contact
    Uri mContactUri;
    // An adapter that binds the result Cursor to the ListView
    private SimpleCursorAdapter mCursorAdapter;

    private static final String[] CONTACTS_SUMMARY_PROJECTION = new String[] {
            ContactsContract.Contacts._ID,
            ContactsContract.Contacts.DISPLAY_NAME,
            ContactsContract.Contacts.HAS_PHONE_NUMBER,
            ContactsContract.Contacts.LOOKUP_KEY
    };

    public InviteFriendsActivity() {}

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.invite_friends_view);

        ActionBar actionBar = getActionBar();
        actionBar.hide();

        Cursor c = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, CONTACTS_SUMMARY_PROJECTION , null, null, ContactsContract.Contacts.DISPLAY_NAME + " ASC");

        // Gets the ListView from the View list of the parent activity
        mContactsList = (ListView) findViewById(android.R.id.list);

        // Sets the adapter for the ListView
        mContactsList.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);

        mContactsList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                onLongListItemClick(view, position, id);
                return false;
            }
        });

        // Gets a CursorAdapter
        mCursorAdapter = new SimpleCursorAdapter(
                this,
                R.layout.item_invite_friends,
                c,
                FROM_COLUMNS, TO_IDS,
                0);

        mContactsList.setAdapter(mCursorAdapter);

        Log.i("TEEEEST: ", "Adaoter set. " + mContactsList.getAdapter().getCount());
    }

    /**
     *
     * @param view
     * @param position
     * @param id
     */
    protected void onLongListItemClick(View view, int position, long id) {
        getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        getListView().setMultiChoiceModeListener(new ModeCallback());
        getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View item,
                                           int position, long id) {
                getListView().setItemChecked(position, true);
                return true;
            }

        });
    }

    public SimpleCursorAdapter getAdapter()
    {
        return this.mCursorAdapter;
    }

    /**
     *
     * @param phoneNo
     * @param message
     */
    protected void sendSMSMessage(String phoneNo, String message) {
        Log.i("Send SMS", "");

        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, message, null, null);
            Toast.makeText(getApplicationContext(), "SMS sent.",
                    Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(),
                    "SMS faild, please try again.",
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    /**
     *
     * @param id
     * @return
     */
    public String getPhoneNumber(String id)
    {
        String number = "";
        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone._ID + " = " + id, null, null);

        if(phones.getCount() > 0)
        {
            while(phones.moveToNext())
            {
                number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            }

        }
        Log.i("NUMBEEEEEER: ", number);
        phones.close();

        return number;
    }

    private class ModeCallback implements ListView.MultiChoiceModeListener {

        /**
         * Edit action mode
         *
         * @param mode
         * @param menu
         * @return
         */
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_invite_friends, menu);
            mode.setTitle("Select Items");
            return true;
        }

        /**
         *
         * @param mode
         * @param menu
         * @return
         */
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return true;
        }

        /**
         * Click on action menu buttons
         *
         * @param mode
         * @param item
         * @return
         */
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menu_share:
                    SparseBooleanArray checked = getListView().getCheckedItemPositions();

                    for (int i = 0; i < getListView().getAdapter().getCount(); i++) {
                        TravelDAO dao = new TravelDAO(InviteFriendsActivity.this);
                        if (checked.get(i)) {
                            Cursor cursor = (Cursor) getListView().getAdapter().getItem(i);
                            int ColumeIndex_ID = cursor.getColumnIndex(ContactsContract.Contacts._ID);
                            int ColumeIndex_DISPLAY_NAME = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
                            int ColumeIndex_HAS_PHONE_NUMBER = cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER);
                            String id = cursor.getString(ColumeIndex_ID);
                            String name = cursor.getString(ColumeIndex_DISPLAY_NAME);
                            String has_phone = cursor.getString(ColumeIndex_HAS_PHONE_NUMBER);

                            if(!has_phone.trim().equals("0")) {
                                String message = String.format(getResources().getString(R.string.invite_friend_sms), name);
                                sendSMSMessage(getPhoneNumber(id), message);
                            }
                        }
                    }
                    mode.finish();
                    break;
                default:
                    Toast.makeText(InviteFriendsActivity.this, "Clicked " + item.getTitle(),
                            Toast.LENGTH_SHORT).show();
                    break;
            }
            return true;
        }

        /**
         * What happens when the "Done" button is pressed, or no item is selected
         *
         * @param mode
         */
        public void onDestroyActionMode(ActionMode mode) {
            // I am sure there is a better way to do this, but I could not find one
            Intent intent = new Intent(InviteFriendsActivity.this, InviteFriendsActivity.class);
            startActivity(intent);
            finish();
        }

        /**
         * List item selection
         *
         * @param mode
         * @param position
         * @param id
         * @param checked
         */
        public void onItemCheckedStateChanged(ActionMode mode,
                                              int position, long id, boolean checked) {
            final int checkedCount = getListView().getCheckedItemCount();
            switch (checkedCount) {
                case 0:
                    mode.setSubtitle(null);
                    break;
                case 1:
                    mode.setSubtitle("One item selected");
                    break;
                default:
                    mode.setSubtitle("" + checkedCount + " items selected");
                    break;
            }
        }
    }

}
