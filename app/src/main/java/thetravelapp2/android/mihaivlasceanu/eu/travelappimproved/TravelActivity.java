package thetravelapp2.android.mihaivlasceanu.eu.travelappimproved;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import thetravelapp2.android.mihaivlasceanu.eu.travelappimproved.DAO.TravelDAO;


public class TravelActivity extends ActionBarActivity {

    // Key strings for instances
    public static final String LAST_START = "LAST_START";
    public static final String LAST_DESTINATION = "LAST_DESTINATION";
    public static final String SELECTED_STATION_NAME = "SELECTED_STATION_NAME";
    public static final String CHECKED_IN = "CHECKED_IN";
    public static final String CHECKED_OUT = "CHECKED_OUT";

    // Store trip data
    private String check_in;
    private String check_out;

    private boolean checkedIn = false;
    private boolean checkedOut = false;

    // Check-in button
    private Button check_in_button;

    // Check-out button
    private Button check_out_button;

    // Check-in station input
    private EditText start_station;

    // Check-out station input
    private EditText stop_station;

    // Select start button
    private Button select_check_in;

    // Select stop button
    private Button select_check_out;

    // Train logo
    private ImageView logo;

    // Dao instance
    private TravelDAO dao;

    // Preferences instance
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        preferences = this.getSharedPreferences(this.getPackageName() + "_preferences", Context.MODE_PRIVATE);
        init();
    }

    /**
     * Initialize all form items
     */
    private void init()
    {
        start_station       = (start_station == null ? (EditText) findViewById(R.id.input_start_station) : start_station);
        stop_station        = (stop_station == null ? (EditText) findViewById(R.id.input_stop_station) : stop_station);
        check_in_button     = (check_in_button == null ? (Button) findViewById(R.id.button_check_in) : check_in_button);
        check_out_button    = (check_out_button == null ? (Button) findViewById(R.id.button_check_out) : check_out_button);
        select_check_in     = (select_check_in == null ? (Button) findViewById(R.id.button_check_in_select) : select_check_in);
        select_check_out    = (select_check_out == null ? (Button) findViewById(R.id.button_check_out_select) : select_check_out);
        watchInputs();
        watchButtons();
    }

    /**
     * Listeners for EditTexts
     */
    private void watchInputs()
    {
        if(isCheckedIn()) {
            start_station.clearFocus();
            start_station.setEnabled(false);
            stop_station.clearFocus();
            stop_station.setEnabled(false);
        };

        // Check-in button
        start_station.setEnabled(!isCheckedIn());

        if(!isCheckedOut() && isCheckedIn())
        {
            // Check-out text
            stop_station.setEnabled(true);

            stop_station.requestFocus();
        }

        // Changed text listener for check-in
        start_station.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                // Enable check-in button only if input is not empty
                if(s.length() > 0) {
                    check_in_button.setEnabled(true);
                } else {
                    check_in_button.setEnabled(false);
                }
            }
        });

        // Listener for checkout input
        stop_station.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                // Enable check-out button only if input is not empty
                if(s.length() > 0) {
                    check_out_button.setEnabled(true);
                } else {
                    check_out_button.setEnabled(false);
                }
            }
        });
    }

    /**
     * Listeners for check in and check out buttons
     */
    private void watchButtons()
    {
        // Check-in button
        check_in_button.setEnabled(!isCheckedIn());

        // Check-out button
        check_out_button.setEnabled((!isCheckedOut() && isCheckedIn()));

        // Select start button
        select_check_in.setEnabled(!isCheckedIn());

        // Select stop button
        select_check_out.setEnabled((!isCheckedOut() && isCheckedIn()));

        // Image
        logo = (ImageView) findViewById(R.id.train_img);


        // Train logo
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent webIntent = new Intent(TravelActivity.this, WebActivity.class);
                startActivity(webIntent);
            }
        });

        dao = new TravelDAO(this);

        // Listener for check-in button
        check_in_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(start_station.getText().length() > 0)
                {
                    // Save the check-in station name
                    TravelActivity.this.check_in = start_station.getText().toString();

                    changeCheckInState(false);

                    changeCheckOutState(true);

                    // Check in flag
                    setCheckedIn(true);

                } else {
                    // Show error message
                    message("Please enter a check-in station name");

                    changeCheckInState(true);

                    changeCheckOutState(false);

                    // Check in flag
                    setCheckedIn(false);

                }
            }
        });

        // Check-out button
        check_out_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(stop_station.getText().length() > 0)
                {
                    // Save check-out station
                    TravelActivity.this.check_out = stop_station.getText().toString();

                    changeCheckInState(false);

                    changeCheckOutState(false);

                    // Checked-out flag
                    setCheckedOut(true);

                    // Save/Not save station
                    Boolean save = preferences.getBoolean("save_stations", true);

                    if(save) {
                        dao.saveStation(TravelActivity.this.check_in);
                        dao.saveStation(TravelActivity.this.check_out);
                    }
                    dao.saveTravel(TravelActivity.this.check_in, TravelActivity.this.check_out);
                    dao.close();


                    // Success message
                    message("Trip finished. Please check your receipt!");
                } else {

                    // Error message
                    message("Please enter a check-out station name");

                    changeCheckInState(false);

                    changeCheckOutState(true);

                    // Checked-out flag
                    setCheckedOut(false);
                }
            }
        });

        select_check_in.setOnClickListener(new SelectStationListener(this, LAST_START));

        select_check_out.setOnClickListener(new SelectStationListener(this, LAST_DESTINATION));
    }

    /**
     * Menu items
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // Receipt menu item
        MenuItem item = menu.add(Menu.NONE, Menu.FIRST, Menu.NONE, "Receipt");
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        // New trip / reset menu item
        MenuItem newTrip = menu.add(Menu.NONE, 2, Menu.NONE, "New Trip");
        newTrip.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return true;
    }

    /**
     * Enables/Disables all the items related to check-in
     *
     * @param enable
     */
    public void changeCheckInState(boolean enable)
    {
        if(enable) {
            start_station.setEnabled(enable);
            start_station.requestFocus();
            select_check_in.setEnabled(true);
            if (start_station.getText().length() > 0) {
                check_in_button.setEnabled(true);
            }
        } else {
            start_station.clearFocus();
            check_in_button.setEnabled(false);
            select_check_in.setEnabled(false);
            start_station.setEnabled(false);
        }
    }

    /**
     * Enables/Disables all the items related to check-out
     *
     * @param enable
     */
    public void changeCheckOutState(boolean enable)
    {
        if(enable) {
            stop_station.setEnabled(enable);
            stop_station.requestFocus();
            select_check_out.setEnabled(true);
            if (stop_station.getText().length() > 0) {
                check_out_button.setEnabled(true);
            }
        } else {
            stop_station.clearFocus();
            check_out_button.setEnabled(false);
            select_check_out.setEnabled(false);
            stop_station.setEnabled(false);
        }
    }


    /**
     * Select action on menu items
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        // History menu clicked
        if(id == R.id.history)
        {
            Intent intent = new Intent(this, TravelHistoryActivity.class);
            startActivity(intent);
        }

        // Settings menu clicked
        if(id == R.id.settings)
        {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }

        if(id == R.id.invite)
        {
            Intent intent = new Intent(this, InviteFriendsActivity.class);
            startActivity(intent);
        }

        // Show receipt activity
        if (id == Menu.FIRST) {
            if(check_in != null && check_out != null)
            {
                Intent intent = new Intent(this, ReceiptActivity.class);
                intent.putExtra(LAST_START, check_in);
                intent.putExtra(LAST_DESTINATION, check_out);
                startActivity(intent);
            } else {
                message("You have not checked out yet!");
                return false;
            }
        }

        // New trip
        if (id == 2) {
            Intent intent = getIntent();
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            finish();
            startActivity(intent);
            overridePendingTransition(0, 0);
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Save instance data
     *
     * @param outState
     */
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(LAST_START, check_in);
        outState.putString(LAST_DESTINATION, check_out);
        outState.putBoolean(CHECKED_IN, isCheckedIn());
        outState.putBoolean(CHECKED_OUT, isCheckedOut());
    }

    /**
     * Restore state behavior
     *
     * @param savedInstanceState
     */
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        check_in = savedInstanceState.getString(LAST_START);
        check_out = savedInstanceState.getString(LAST_DESTINATION);
        setCheckedIn(savedInstanceState.getBoolean(CHECKED_IN));
        setCheckedOut(savedInstanceState.getBoolean(CHECKED_OUT));
        watchInputs();
        watchButtons();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if(resultCode == RESULT_FIRST_USER)
        {
            if(intent != null)
            {
                String action = intent.getStringExtra("ACTION");
                if(action != null && action.length() > 0)
                {
                    if(action.equalsIgnoreCase(LAST_DESTINATION))
                    {
                        String selected = intent.getStringExtra(SELECTED_STATION_NAME);

                        if(selected != null && selected.length() > 0) {
                            // Check-out station input
                            stop_station.setText(selected);
                        }
                    } else if(action.equalsIgnoreCase(LAST_START)) {
                        String selected = intent.getStringExtra(SELECTED_STATION_NAME);

                        if(selected != null && selected.length() > 0)
                        {
                            // Check-in station input
                            start_station.setText(selected);
                        }
                    } else {
                        message("No station selected (1).");
                    }
                } else {
                    message("There was an error when trying to retrieve the station action. Please try again.");
                }
            } else {
                message("There was an error when trying to retrieve the station. Please try again.");
            }
        } else {
            message("No station selected.");
        }
    }

    public void setCheckedIn(boolean checkedIn) {
        this.checkedIn = checkedIn;
    }

    public void setCheckedOut(boolean checkedOut) {
        this.checkedOut = checkedOut;
    }

    public boolean getCheckedIn()
    {
        return this.checkedIn;
    }

    public boolean getCheckedOut()
    {
        return this.checkedOut;
    }

    public boolean isCheckedIn()
    {
        return getCheckedIn();
    }

    public boolean isCheckedOut()
    {
        return getCheckedOut();
    }

    /**
     * Toast shortcut
     *
     * @param text
     */
    public void message(String text)
    {
        Toast.makeText(TravelActivity.this, text, Toast.LENGTH_SHORT).show();
    }
}
