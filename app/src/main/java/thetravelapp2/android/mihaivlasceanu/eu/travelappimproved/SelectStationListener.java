package thetravelapp2.android.mihaivlasceanu.eu.travelappimproved;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

/**
 * Created by Rembrandt on 03-Mar-15.
 */
public class SelectStationListener implements View.OnClickListener
{
    /**
     * Action path
     */
    public static String SELECT_DESTINATION_ACTION = "thetravelapp2.android.mihaivlasceanu.eu.travelappimproved.SELECT_DESTINATION_ACTION";

    /**
     * Referenced activity
     */
    private Activity activity;

    // Response code
    private String action;

    /**
     * Constructor with Activity reference and the response code
     * @param activity
     * @param action
     */
    public SelectStationListener(Activity activity, String action)
    {
        this.setActivity(activity);
        this.setAction(action);
    }

    /**
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        Intent destination = new Intent(activity, DestinationActivity.class);
        destination.putExtra("ACTION", getAction());
        getActivity().startActivityForResult(destination, Activity.RESULT_FIRST_USER);
    }

    /**
     * Getter
     * @return
     */
    public Activity getActivity() {
        return activity;
    }

    /**
     * Setter
     * @param activity
     */
    public final void setActivity(Activity activity) {
        this.activity = activity;
    }

    /**
     *
     * @return
     */
    public String getAction() {
        return action;
    }

    /**
     *
     * @param action
     */
    public final void setAction(String action) {
        this.action = action;
    }
}
